# RPG Character Generator

## Table of Contents

- [Description](#description)
- [Install](#install)
- [Usage](#usage)
- [ProgramFAQ](#programfaq)
- [TestCoverage](#testcoverage)
- [Maintainer](#maintainer)
- [License](#license)

## Description

Assignment 1

## Install

No installation needed, all dependencies are downloaded through gradle.

## Usage

Open project in Intellij and run. Requires Java 17.

## ProgramFAQ
Program is able to create 4 types of RPG Characters. Characters have shared and unique items. Weapons and armor. Characters also have three attributes (strength, dexterity, intelligence). Characters also have main attribute.
As an example; Mage has starting attributes of strength=1, dexterity=1 and intelligence=8. Intelligence is Mages main attribute. Main attributes increase damage. All functionality required in assignment description is tested and working.

## TestCoverage

Testing covers the following:
- Character creation; name, level, base attributes
- Character leveling; leveling, attribute enhancements
- Attribute enhancements when armor is equipped
- Damage per second calculations; with and without weapons equipped
- Weapon creation; name, damage, required level, weapon type, item slot
- Armor creation; name, required level, armor type, item slot
- Equip; required level
- Exceptions; InvalidWeaponException, InvalidArmorException (tested in WarriorItemTest)

## Maintainer

Tino Nummela | [@tinonum](https://github.com/tinonum)

## License

MIT © 2022 Tino Nummela
