package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorTest {

    // Testing that Warrior name 'Warru' is correct
    @Test
    public void warrior_Name_shouldReturnCorrectName() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        String expected = "Warru";
        // Act - calling the method
        String actual = warrior.getName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior starting level is correct
    @Test
    public void warrior_startLevel_shouldReturnCorrectLevel() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        int expected = 1;
        // Act - calling the method
        int actual = warrior.getLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior base strength is correct
    @Test
    public void warrior_baseStrength_shouldReturnCorrectBaseStrength() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        double expected = 5.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior base dexterity is correct
    @Test
    public void warrior_baseDexterity_shouldReturnCorrectBaseDexterity() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        double expected = 2.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior base intelligence is correct
    @Test
    public void warrior_baseIntelligence_shouldReturnCorrectBaseIntelligence() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        double expected = 1.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Strength enhances correctly during levelUp()
    @Test
    public void warrior_levelUpStrength_shouldReturnEnhancedStrength() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.levelUp();
        double expected = 8.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Dexterity enhances correctly during levelUp()
    @Test
    public void warrior_levelUpDexterity_shouldReturnEnhancedDexterity() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.levelUp();
        double expected = 4.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Intelligence enhances correctly during two times levelUp()
    @Test
    public void warrior_levelUpIntelligence_shouldReturnEnhancedIntelligence() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.levelUp();
        warrior.levelUp();
        double expected = 3.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Warrior Strength enhances when Head Armor is equipped (with one level up)
    @Test
    public void warrior_strengthEnhancementWithHeadArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.levelUp();
        Armor headArmor = new Armor("Common Head Plate", 1, ArmorType.PLATE, Slot.HEAD);
        warrior.equipArmor(headArmor);
        double expected = 13.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Warrior Strength enhances when Body Armor is equipped
    @Test
    public void warrior_strengthEnhancementWithBodyArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        Armor bodyArmor = new Armor("Common Head Plate", 1, ArmorType.PLATE, Slot.BODY);
        warrior.equipArmor(bodyArmor);
        double expected = 10.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Warrior Strength enhances when Leg Armor is equipped (with one level up)
    @Test
    public void warrior_strengthEnhancementLegArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.levelUp();
        Armor legArmor = new Armor("Common Head Plate", 1, ArmorType.MAIL, Slot.LEG);
        warrior.equipArmor(legArmor);
        double expected = 13.0;
        // Act - calling the method
        double actual = warrior.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Warrior character DPS without weapon equipped (1.05)
    @Test
    public void warrior_dpsWithoutWeapon_shouldReturnCorrectDps() {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        warrior.dpsWithoutItems();
        double expected = 1.05;
        //double
        // Act - calling the method
        double actual = warrior.characterDps.getTotalDps();
        //do
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Warrior character DPS with items equipped (8.47)
    @Test
    public void warrior_dpsWithWeapon_shouldReturnCorrectDps() throws InvalidArmorException, InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        Weapon weapon = new Weapon("Common Axe",7, 1, WeaponType.AXE, Slot.ARM);
        Armor armor = new Armor("Common Body Plate", 1, ArmorType.PLATE, Slot.BODY);
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);
        warrior.dpsWithItems(weapon, armor);
        double expected = 8.47;
        // Act - calling the method
        double actual = warrior.characterDps.getTotalDps();
        // Assert - verify
        assertEquals(expected, actual);
    }
}