package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerTest {

    // Testing that Ranger name 'Rangeri' is correct
    @Test
    public void ranger_Name_shouldReturnCorrectName() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        String expected = "Rangeri";
        // Act - calling the method
        String actual = ranger.getName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger starting level is correct
    @Test
    public void ranger_startLevel_shouldReturnCorrectLevel() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        int expected = 1;
        // Act - calling the method
        int actual = ranger.getLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger base strength is correct
    @Test
    public void ranger_baseStrength_shouldReturnCorrectBaseStrength() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        double expected = 1.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger base dexterity is correct
    @Test
    public void ranger_baseDexterity_shouldReturnCorrectBaseDexterity() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        double expected = 7.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger base intelligence is correct
    @Test
    public void ranger_baseIntelligence_shouldReturnCorrectBaseIntelligence() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        double expected = 1.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Strength enhances correctly during levelUp()
    @Test
    public void ranger_levelUpStrength_shouldReturnEnhancedStrength() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.levelUp();
        double expected = 2.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Dexterity enhances correctly during levelUp()
    @Test
    public void ranger_levelUpDexterity_shouldReturnEnhancedDexterity() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.levelUp();
        double expected = 12.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Intelligence enhances correctly during levelUp()
    @Test
    public void ranger_levelUpIntelligence_shouldReturnEnhancedIntelligence() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.levelUp();
        double expected = 2.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Ranger Strength enhances when Head Armor is equipped (with one level up)
    @Test
    public void ranger_strengthEnhancementWithHeadArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.levelUp();
        Armor headArmor = new Armor("Common Head Mail", 1, ArmorType.MAIL, Slot.HEAD);
        ranger.equipArmor(headArmor);
        double expected = 7.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Ranger Strength enhances when Body Armor is equipped (with one level up)
    @Test
    public void ranger_strengthEnhancementWithBodyArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.levelUp();
        Armor bodyArmor = new Armor("Common Body Mail", 1, ArmorType.MAIL, Slot.BODY);
        ranger.equipArmor(bodyArmor);
        double expected = 7.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Ranger Strength enhances when Leg Armor is equipped
    @Test
    public void ranger_strengthEnhancementWithLegArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        Armor legArmor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        ranger.equipArmor(legArmor);
        double expected = 6.0;
        // Act - calling the method
        double actual = ranger.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Warrior character DPS without weapon equipped (1.07)
    @Test
    public void warrior_dpsWithoutWeapon_shouldReturnCorrectDps() {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        ranger.dpsWithoutItems();
        double expected = 1.07;
        //double
        // Act - calling the method
        double actual = ranger.characterDps.getTotalDps();
        //do
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Warrior character DPS with items equipped (7.06)
    @Test
    public void warrior_dpsWithWeapon_shouldReturnCorrectDps() throws InvalidArmorException, InvalidWeaponException {
        // Arrange - organizing all the data
        Ranger ranger = new Ranger("Rangeri");
        Weapon weapon = new Weapon("Common Bow",6, 1, WeaponType.BOW, Slot.ARM);
        Armor armor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        ranger.equipWeapon(weapon);
        ranger.equipArmor(armor);
        ranger.dpsWithItems(weapon, armor);
        double expected = 7.06;
        // Act - calling the method
        double actual = ranger.characterDps.getTotalDps();
        // Assert - verify
        assertEquals(expected, actual);
    }
}