package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueTest {

    // Testing that Rogue name 'Rogqu' is correct
    @Test
    public void rogue_Name_shouldReturnCorrectName() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        String expected = "Roggu";
        // Act - calling the method
        String actual = rogue.getName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue starting level is correct
    @Test
    public void rogue_startLevel_shouldReturnCorrectLevel() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        int expected = 1;
        // Act - calling the method
        int actual = rogue.getLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue base strength is correct
    @Test
    public void rogue_baseStrength_shouldReturnCorrectBaseStrength() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        double expected = 2.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue base dexterity is correct
    @Test
    public void rogue_baseDexterity_shouldReturnCorrectBaseDexterity() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        double expected = 6.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue base intelligence is correct
    @Test
    public void rogue_baseIntelligence_shouldReturnCorrectBaseIntelligence() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        double expected = 1.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Strength enhances correctly during levelUp()
    @Test
    public void rogue_levelUpStrength_shouldReturnEnhancedStrength() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        rogue.levelUp();
        double expected = 3.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Dexterity enhances correctly during levelUp()
    @Test
    public void rogue_levelUpDexterity_shouldReturnEnhancedDexterity() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        rogue.levelUp();
        double expected = 10.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Intelligence enhances correctly during levelUp()
    @Test
    public void rogue_levelUpIntelligence_shouldReturnEnhancedIntelligence() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        rogue.levelUp();
        double expected = 2.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Rogue Strength enhances when Head Armor is equipped
    @Test
    public void rogue_strengthEnhancementWithHeadArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        Armor headArmor = new Armor("Rare Head Leather", 1, ArmorType.LEATHER, Slot.HEAD);
        rogue.equipArmor(headArmor);
        double expected = 7.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Rogue Strength enhances when Body Armor is equipped
    @Test
    public void rogue_strengthEnhancementWithBodyArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        Armor bodyArmor = new Armor("Rare Head Mail", 1, ArmorType.MAIL, Slot.BODY);
        rogue.equipArmor(bodyArmor);
        double expected = 7.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Rogue Strength enhances when Leg Armor is equipped (with one level up)
    @Test
    public void rogue_strengthEnhancementWithLegArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        rogue.levelUp();
        Armor legArmor = new Armor("Rare Head Mail", 1, ArmorType.MAIL, Slot.LEG);
        rogue.equipArmor(legArmor);
        double expected = 8.0;
        // Act - calling the method
        double actual = rogue.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Rogue character DPS without weapon equipped (1.06)
    @Test
    public void rogue_dpsWithoutWeapon_shouldReturnCorrectDps() {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        rogue.dpsWithoutItems();
        double expected = 1.06;
        //double
        // Act - calling the method
        double actual = rogue.characterDps.getTotalDps();
        //do
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Rogue character DPS with items (4.66)
    @Test
    public void rogue_dpsWithWeapon_shouldReturnCorrectDps() throws InvalidArmorException, InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue rogue = new Rogue("Roggu");
        Weapon weapon = new Weapon("Common Bow",4, 1, WeaponType.DAGGER, Slot.ARM);
        Armor armor = new Armor("Common Body Leather", 1, ArmorType.LEATHER, Slot.BODY);
        rogue.equipWeapon(weapon);
        rogue.equipArmor(armor);
        rogue.dpsWithItems(weapon, armor);
        double expected = 4.66;
        // Act - calling the method
        double actual = rogue.characterDps.getTotalDps();
        // Assert - verify
        assertEquals(expected, actual);
    }
}