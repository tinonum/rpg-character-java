package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageTest {

    // Testing that Mage name 'Maagi' is correct
    @Test
    public void mage_Name_shouldReturnCorrectName() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        String expected = "Maagi";
        // Act - calling the method
        String actual = mage.getName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage starting level is correct
    @Test
    public void mage_startLevel_shouldReturnCorrectLevel() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        int expected = 1;
        // Act - calling the method
        int actual = mage.getLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage base strength is correct
    @Test
    public void mage_baseStrength_shouldReturnCorrectBaseStrength() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        double expected = 1.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage base dexterity is correct
    @Test
    public void mage_baseDexterity_shouldReturnCorrectBaseDexterity() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        double expected = 1.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage base intelligence is correct
    @Test
    public void mage_baseIntelligence_shouldReturnCorrectBaseIntelligence() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        double expected = 8.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that level up increases level by 1
    @Test
    public void mage_levelUp_shouldReturnOneLevelHigherMage() {
        Mage mage = new Mage("Maagi");
        mage.levelUp();
        int expected = 2;
        // Act - calling the method
        int actual = mage.getLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Strength enhances correctly during levelUp()
    @Test
    public void mage_levelUpStrength_shouldReturnEnhancedStrength() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        mage.levelUp();
        double expected = 2.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Dexterity enhances correctly during levelUp()
    @Test
    public void mage_levelUpDexterity_shouldReturnEnhancedDexterity() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        mage.levelUp();
        double expected = 2.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getDexterity();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Intelligence enhances correctly during two times levelUp()
    @Test
    public void mage_levelUpIntelligence_shouldReturnEnhancedAttributes() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        mage.levelUp();
        mage.levelUp();
        double expected = 18.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getIntelligence();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Mage Strength enhances when Head Armor is equipped
    @Test
    public void mage_strengthEnhancementWithHeadArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        Armor headArmor = new Armor("Rare Head Cloth", 1, ArmorType.CLOTH, Slot.HEAD);
        mage.equipArmor(headArmor);
        double expected = 6.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Mage Strength enhances when Body Armor is equipped
    @Test
    public void mage_strengthEnhancementWithBodyArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        Armor bodyArmor = new Armor("Rare Head Cloth", 1, ArmorType.CLOTH, Slot.BODY);
        mage.equipArmor(bodyArmor);
        double expected = 6.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing if Mage Strength enhances when Leg Armor is equipped (with one level up)
    @Test
    public void mage_strengthEnhancementWithLegArmor_shouldReturnEnhancedStrength() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        mage.levelUp();
        Armor legArmor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.LEG);
        mage.equipArmor(legArmor);
        double expected = 7.0;
        // Act - calling the method
        double actual = mage.baseAttributes.getStrength();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Mage character DPS without weapon equipped (1.08)
    @Test
    public void mage_dpsWithoutWeapon_shouldReturnCorrectDps() {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        mage.dpsWithoutItems();
        double expected = 1.08;
        //double
        // Act - calling the method
        double actual = mage.characterDps.getTotalDps();
        //do
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing for Mage character DPS with items equipped (4.75)
    @Test
    public void mage_dpsWithWeapon_shouldReturnCorrectDps() throws InvalidArmorException, InvalidWeaponException {
        // Arrange - organizing all the data
        Mage mage = new Mage("Maagi");
        Weapon weapon = new Weapon("Common Wand",4, 1, WeaponType.WAND, Slot.ARM);
        Armor armor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.LEG);
        mage.equipWeapon(weapon);
        mage.equipArmor(armor);
        mage.dpsWithItems(weapon, armor);
        double expected = 4.75;
        // Act - calling the method
        double actual = mage.characterDps.getTotalDps();
        // Assert - verify
        assertEquals(expected, actual);
    }
}
