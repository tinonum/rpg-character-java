package no.accelerate.exception;

import no.accelerate.characters.Warrior;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ExceptionTest {
    // Testing InvalidWeaponException
    @Test
    public void warrior_equipInvalidWeapon_shouldThrowInvalidWeaponException() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        Weapon tryEquipBow = new Weapon("Common Bow",2, 1, WeaponType.BOW, Slot.ARM);
        // Act - calling the method + Assert - verify
        assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(tryEquipBow));
    }

    // Testing InvalidArmorException
    @Test
    public void warrior_equipInvalidArmor_shouldThrowInvalidArmorException() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior warrior = new Warrior("Warru");
        Armor tryEquipCloth = new Armor("Common Head Cloth",1, ArmorType.CLOTH,Slot.HEAD);
        // Act - calling the method + Assert - verify
        assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(tryEquipCloth));

    }
}
