package no.accelerate.items;

import no.accelerate.characters.Rogue;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static no.accelerate.item.ArmorType.LEATHER;
import static no.accelerate.item.WeaponType.DAGGER;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueItemTest {
    // Testing that Rogue Weapon 'DAGGER' type is correct
    @Test
    public void rogue_weapon_type_shouldReturnCorrectWeaponType() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue testDagger = new Rogue("Roggu");
        Weapon dagger = new Weapon("Common Dagger",2, 1, WeaponType.DAGGER, Slot.ARM);
        testDagger.equipWeapon(dagger);
        Enum expected = DAGGER;
        // Act - calling the method
        Enum actual = dagger.getWeaponType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Weapon 'DAGGER' name is correct
    @Test
    public void rogue_weapon_name_shouldReturnCorrectWeaponName() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue testDagger = new Rogue("Roggu");
        Weapon dagger = new Weapon("Common Dagger",2, 1, WeaponType.DAGGER, Slot.ARM);
        testDagger.equipWeapon(dagger);
        String expected = "Common Dagger";
        // Act - calling the method
        String actual = dagger.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Weapon 'DAGGER' required level is correct
    @Test
    public void rogue_weapon_level_shouldReturnCorrectWeaponRequiredLevel() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue testDagger = new Rogue("Roggu");
        Weapon dagger = new Weapon("Common Dagger",2, 1, WeaponType.DAGGER, Slot.ARM);
        testDagger.equipWeapon(dagger);
        int expected = 1;
        // Act - calling the method
        int actual = dagger.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Weapon 'DAGGER' slot is correct
    @Test
    public void rogue_weapon_slot_shouldReturnCorrectWeaponSlot() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue testDagger = new Rogue("Roggu");
        Weapon dagger = new Weapon("Common Dagger",2, 1, WeaponType.DAGGER, Slot.ARM);
        testDagger.equipWeapon(dagger);
        Enum expected = Slot.ARM;
        // Act - calling the method
        Enum actual = dagger.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Leg Armor 'LEATHER' slot is correct
    @Test
    public void rogue_legArmor_slot_shouldReturnCorrectArmorSlot() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue testLegArmor = new Rogue("Roggu");
        Armor legArmor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        Enum expected = Slot.LEG;
        // Act - calling the method
        Enum actual = legArmor.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Leg Armor 'LEATHER' type is correct
    @Test
    public void rogue_legArmor_type_shouldReturnCorrectArmorType() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue testLegArmor = new Rogue("Roggu");
        Armor legArmor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        Enum expected = LEATHER;
        // Act - calling the method
        Enum actual = legArmor.getArmorType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Leg Armor 'LEATHER' name is correct
    @Test
    public void rogue_legArmor_name_shouldReturnCorrectArmorName() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue testLegArmor = new Rogue("Roggu");
        Armor legArmor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        String expected = "Common Leg Leather";
        // Act - calling the method
        String actual = legArmor.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Leg Armor 'LEATHER' required level is correct
    @Test
    public void rogue_legArmor_level_shouldReturnCorrectArmorRequiredLevel() throws InvalidArmorException {
        // Arrange - organizing all the data
        Rogue testLegArmor = new Rogue("Roggu");
        Armor legArmor = new Armor("Common Leg Leather", 1, ArmorType.LEATHER, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        int expected = 1;
        // Act - calling the method
        int actual = legArmor.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue level is high enough to equip 'SWORD' (level must be at least 4)
    @Test
    public void warrior_equipHammer_levelRequirement_shouldReturnHammerCorrectlyEquipped() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Rogue testSwordLevel = new Rogue("Roggu");
        testSwordLevel.levelUp();
        testSwordLevel.levelUp();
        testSwordLevel.levelUp();
        Weapon sword = new Weapon("Rare Sword",4, 4, WeaponType.SWORD, Slot.ARM);
        testSwordLevel.equipWeapon(sword);
        int expected = 4;
        // Act - calling the method
        int actual = sword.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }
}