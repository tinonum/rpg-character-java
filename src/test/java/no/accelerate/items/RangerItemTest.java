package no.accelerate.items;

import no.accelerate.characters.Mage;
import no.accelerate.characters.Ranger;
import no.accelerate.characters.Rogue;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static no.accelerate.item.ArmorType.LEATHER;
import static no.accelerate.item.ArmorType.MAIL;
import static no.accelerate.item.WeaponType.BOW;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerItemTest {
    // Testing that Ranger Weapon 'BOW' type is correct
    @Test
    public void ranger_weapon_type_shouldReturnCorrectWeaponType() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Ranger testBow = new Ranger("Ranger");
        Weapon bow = new Weapon("Common Bow",2, 1, WeaponType.BOW, Slot.ARM);
        testBow.equipWeapon(bow);
        Enum expected = BOW;
        // Act - calling the method
        Enum actual = bow.getWeaponType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Weapon 'BOW' name is correct
    @Test
    public void ranger_weapon_name_shouldReturnCorrectWeaponName() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Ranger testBow = new Ranger("Ranger");
        Weapon bow = new Weapon("Common Bow",2, 1, WeaponType.BOW, Slot.ARM);
        testBow.equipWeapon(bow);
        String expected = "Common Bow";
        // Act - calling the method
        String actual = bow.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Weapon 'BOW' required level is correct
    @Test
    public void ranger_weapon_level_shouldReturnCorrectWeaponRequiredLevel() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Ranger testBow = new Ranger("Ranger");
        Weapon bow = new Weapon("Common Bow",2, 1, WeaponType.BOW, Slot.ARM);
        testBow.equipWeapon(bow);
        int expected = 1;
        // Act - calling the method
        int actual = bow.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Weapon 'BOW' slot is correct
    @Test
    public void ranger_weapon_slot_shouldReturnCorrectWeaponSlot() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Ranger testBow = new Ranger("Rangeri");
        Weapon bow = new Weapon("Common Bow",2, 1, WeaponType.BOW, Slot.ARM);
        testBow.equipWeapon(bow);
        Enum expected = Slot.ARM;
        // Act - calling the method
        Enum actual = bow.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Leg Armor 'MAIL' slot is correct
    @Test
    public void ranger_legArmor_slot_shouldReturnCorrectArmorSlot() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger testLegArmor = new Ranger("Rangeri");
        Armor legArmor = new Armor("Common Leg Mail", 1, ArmorType.MAIL, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        Enum expected = Slot.LEG;
        // Act - calling the method
        Enum actual = legArmor.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Leg Armor 'MAIL' type is correct
    @Test
    public void ranger_legArmor_type_shouldReturnCorrectArmorType() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger testLegArmor = new Ranger("Rangeri");
        Armor legArmor = new Armor("Common Leg Mail", 1, ArmorType.MAIL, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        Enum expected = MAIL;
        // Act - calling the method
        Enum actual = legArmor.getArmorType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Ranger Leg Armor 'MAIL' name is correct
    @Test
    public void ranger_legArmor_name_shouldReturnCorrectArmorName() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger testLegArmor = new Ranger("Rangeri");
        Armor legArmor = new Armor("Common Leg Mail", 1, ArmorType.MAIL, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        String expected = "Common Leg Mail";
        // Act - calling the method
        String actual = legArmor.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Rogue Ranger Leg Armor 'MAIL' required level is correct
    @Test
    public void ranger_legArmor_level_shouldReturnCorrectArmorRequiredLevel() throws InvalidArmorException {
        // Arrange - organizing all the data
        Ranger testLegArmor = new Ranger("Rangeri");
        testLegArmor.levelUp();
        Armor legArmor = new Armor("Rare Leg Mail", 2, ArmorType.MAIL, Slot.LEG);
        testLegArmor.equipArmor(legArmor);
        int expected = 2;
        // Act - calling the method
        int actual = legArmor.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }
}
