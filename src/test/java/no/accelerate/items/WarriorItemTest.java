package no.accelerate.items;

import no.accelerate.characters.Mage;
import no.accelerate.characters.Warrior;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static no.accelerate.item.ArmorType.PLATE;
import static no.accelerate.item.WeaponType.AXE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorItemTest {
    // Testing that Warrior Weapon 'AXE' type is correct
    @Test
    public void warrior_weapon_type_shouldReturnCorrectWeaponType() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior testAxe = new Warrior("Warru");
        Weapon axe = new Weapon("Common Axe",2, 1, WeaponType.AXE, Slot.ARM);
        testAxe.equipWeapon(axe);
        Enum expected = AXE;
        // Act - calling the method
        Enum actual = axe.getWeaponType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Weapon 'AXE' name is correct
    @Test
    public void warrior_weapon_name_shouldReturnCorrectWeaponName() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior testAxe = new Warrior("Warru");
        Weapon axe = new Weapon("Common Axe",2, 1, WeaponType.AXE, Slot.ARM);
        testAxe.equipWeapon(axe);
        String expected = "Common Axe";
        // Act - calling the method
        String actual = axe.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Weapon 'AXE' required level is correct
    @Test
    public void warrior_weapon_level_shouldReturnCorrectWeaponRequiredLevel() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior testAxe = new Warrior("Warru");
        Weapon axe = new Weapon("Common Axe",2, 1, WeaponType.AXE, Slot.ARM);
        testAxe.equipWeapon(axe);
        int expected = 1;
        // Act - calling the method
        int actual = axe.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Weapon 'AXE' slot is correct
    @Test
    public void warrior_weapon_slot_shouldReturnCorrectWeaponSlot() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior testAxe = new Warrior("Warru");
        Weapon axe = new Weapon("Common Axe",2, 1, WeaponType.AXE, Slot.ARM);
        testAxe.equipWeapon(axe);
        Enum expected = Slot.ARM;
        // Act - calling the method
        Enum actual = axe.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Weapon 'AXE' damage is correct
    @Test
    public void warrior_weapon_damage_shouldReturnCorrectWeaponDamage() throws InvalidWeaponException {
        // Arrange
        Warrior testAxe = new Warrior("Warru");
        Weapon axe = new Weapon("Common Axe",2,1, WeaponType.AXE, Slot.ARM);
        double expected = 2.0;
        // Act
        double actual = axe.getDamage();
        // Verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Body Armor 'PLATE' slot is correct
    @Test
    public void warrior_bodyArmor_slot_shouldReturnCorrectArmorSlot() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior testBodyArmor = new Warrior("Warru");
        Armor bodyArmor = new Armor("Common Body Plate", 1, ArmorType.PLATE, Slot.BODY);
        testBodyArmor.equipArmor(bodyArmor);
        Enum expected = Slot.BODY;
        // Act - calling the method
        Enum actual = bodyArmor.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Body Armor 'PLATE' type is correct
    @Test
    public void warrior_bodyArmor_type_shouldReturnCorrectArmorType() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior testBodyArmor = new Warrior("Warru");
        Armor bodyArmor = new Armor("Common Body Plate", 1, ArmorType.PLATE, Slot.BODY);
        testBodyArmor.equipArmor(bodyArmor);
        Enum expected = PLATE;
        // Act - calling the method
        Enum actual = bodyArmor.getArmorType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Body Armor 'PLATE' name is correct
    @Test
    public void warrior_bodyArmor_name_shouldReturnCorrectArmorName() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior testBodyArmor = new Warrior("Warru");
        Armor headArmor = new Armor("Common Body Plate", 1, ArmorType.PLATE, Slot.BODY);
        testBodyArmor.equipArmor(headArmor);
        String expected = "Common Body Plate";
        // Act - calling the method
        String actual = headArmor.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior Body Armor 'PLATE' required level is correct
    @Test
    public void warrior_bodyArmor_levelRequirement_shouldReturnCorrectArmorRequiredLevel() throws InvalidArmorException {
        // Arrange - organizing all the data
        Warrior testBodyArmor = new Warrior("Warru");
        testBodyArmor.levelUp();

        Armor bodyArmor = new Armor("Common Body Plate", 2, ArmorType.PLATE, Slot.BODY);
        testBodyArmor.equipArmor(bodyArmor);
        int expected = 2;
        // Act - calling the method
        int actual = bodyArmor.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Warrior level is high enough to equip 'AXE' (level must be at least 4)
    @Test
    public void warrior_equipAxe_levelRequirement_shouldReturnAxeCorrectlyEquipped() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Warrior testHammerLevel = new Warrior("Warru");
        testHammerLevel.levelUp();
        Weapon hammer = new Weapon("Rare Axe",4, 2, WeaponType.AXE, Slot.ARM);
        testHammerLevel.equipWeapon(hammer);
        int expected = 2;
        // Act - calling the method
        int actual = hammer.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }
}