package no.accelerate.items;

import no.accelerate.characters.Mage;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static no.accelerate.item.ArmorType.CLOTH;
import static no.accelerate.item.ArmorType.MAIL;
import static no.accelerate.item.WeaponType.STAFF;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageItemTest {
    // Testing that Mage Weapon 'STAFF' type is correct
    @Test
    public void mage_weapon_type_shouldReturnCorrectWeaponType() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Mage testStaff = new Mage("Maagi");
        Weapon staff = new Weapon("Common Staff",2, 1, WeaponType.STAFF, Slot.ARM);
        testStaff.equipWeapon(staff);
        Enum expected = STAFF;
        // Act - calling the method
        Enum actual = staff.getWeaponType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Weapon 'STAFF' name is correct
    @Test
    public void mage_weapon_name_shouldReturnCorrectWeaponName() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Mage testStaff = new Mage("Maagi");
        Weapon staff = new Weapon("Common Staff",2, 1, WeaponType.STAFF, Slot.ARM);
        testStaff.equipWeapon(staff);
        String expected = "Common Staff";
        // Act - calling the method
        String actual = staff.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Weapon 'STAFF' required level is correct
    @Test
    public void mage_weapon_level_shouldReturnCorrectWeaponRequiredLevel() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Mage testStaff = new Mage("Maagi");
        Weapon staff = new Weapon("Common Staff",2, 1, WeaponType.STAFF, Slot.ARM);
        testStaff.equipWeapon(staff);
        int expected = 1;
        // Act - calling the method
        int actual = staff.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Weapon 'STAFF' slot is correct
    @Test
    public void mage_weapon_slot_shouldReturnCorrectWeaponSlot() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Mage testStaff = new Mage("Maagi");
        Weapon staff = new Weapon("Common Staff",2, 1, WeaponType.STAFF, Slot.ARM);
        testStaff.equipWeapon(staff);
        Enum expected = Slot.ARM;
        // Act - calling the method
        Enum actual = staff.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }


    // Testing that Mage Head Armor 'CLOTH' slot is correct
    @Test
    public void mage_headArmor_slot_shouldReturnCorrectArmorSlot() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage testHeadArmor = new Mage("Maagi");
        Armor headArmor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.HEAD);
        testHeadArmor.equipArmor(headArmor);
        Enum expected = Slot.HEAD;
        // Act - calling the method
        Enum actual = headArmor.getSlot();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Head Armor 'CLOTH' type is correct
    @Test
    public void mage_headArmor_type_shouldReturnCorrectArmorType() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage testHeadArmor = new Mage("Maagi");
        Armor headArmor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.HEAD);
        testHeadArmor.equipArmor(headArmor);
        Enum expected = CLOTH;
        // Act - calling the method
        Enum actual = headArmor.getArmorType();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Head Armor 'CLOTH' name is correct
    @Test
    public void mage_headArmor_name_shouldReturnCorrectArmorName() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage testHeadArmor = new Mage("Maagi");
        Armor headArmor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.HEAD);
        testHeadArmor.equipArmor(headArmor);
        String expected = "Common Head Cloth";
        // Act - calling the method
        String actual = headArmor.getItemName();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage Armor 'CLOTH' required level is correct
    @Test
    public void mage_headArmor_level_shouldReturnCorrectArmorRequiredLevel() throws InvalidArmorException {
        // Arrange - organizing all the data
        Mage testHeadArmor = new Mage("Maagi");
        Armor headArmor = new Armor("Common Head Cloth", 1, ArmorType.CLOTH, Slot.HEAD);
        testHeadArmor.equipArmor(headArmor);
        int expected = 1;
        // Act - calling the method
        int actual = headArmor.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }

    // Testing that Mage level is high enough to equip 'WAND' (level must be at least 4)
    @Test
    public void mage_equipWand_levelRequirement_shouldReturnWandCorrectlyEquipped() throws InvalidWeaponException {
        // Arrange - organizing all the data
        Mage testWandLevel = new Mage("Maagi");
        testWandLevel.levelUp();
        testWandLevel.levelUp();
        testWandLevel.levelUp();
        Weapon wand = new Weapon("Rare Wand",8, 4, WeaponType.WAND, Slot.ARM);
        testWandLevel.equipWeapon(wand);
        int expected = 4;
        // Act - calling the method
        int actual = wand.getRequiredLevel();
        // Assert - verify
        assertEquals(expected, actual);
    }
}