package no.accelerate.item;

public class Armor extends Item {
    private double extraHealth = 5;
    private ArmorType armorType;

    public Armor(String itemName, int requiredLevel, ArmorType armorType, Slot slot) {
        super(itemName, requiredLevel, slot);
        this.armorType = armorType;
        this.extraHealth = extraHealth;
    }

    /* Generated methods */

    // Getters and Setters

    public double getExtraHealth() {
        return extraHealth;
    }

    public void setExtraHealth(double extraHealth) {
        this.extraHealth = extraHealth;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }
}
