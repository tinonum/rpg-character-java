package no.accelerate.item;

public abstract class Item{
    private String itemName;
    private int requiredLevel;

    private Slot slot;

    public Item(String itemName, int requiredLevel, Slot slot) {

        this.itemName = itemName;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /* Generated methods */

    // Getters and Setters

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }
}
