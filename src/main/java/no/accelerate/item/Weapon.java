package no.accelerate.item;

public class Weapon extends Item {
    private WeaponType weaponType;

    private double damage;
    private double dpsWeapon;

    public Weapon(String itemName,double damage, int requiredLevel,WeaponType weaponType, Slot slot) {
        super(itemName, requiredLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.dpsWeapon = dpsWeapon;

    }

    public void calculateWeaponDps() {
        dpsWeapon = damage * 1.1;
    }
    /* Generated methods */

    // Getters and setters

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getDpsWeapon() {
        return dpsWeapon;
    }

    public void setDpsWeapon(double dpsWeapon) {
        this.dpsWeapon = dpsWeapon;
    }
}
