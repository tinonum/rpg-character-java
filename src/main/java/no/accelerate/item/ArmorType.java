package no.accelerate.item;

public enum ArmorType {
    CLOTH, LEATHER, PLATE, MAIL
}
