package no.accelerate.exception;

public class InvalidArmorException extends Throwable {
    public InvalidArmorException(String message) {
        super(message);
    }
}
