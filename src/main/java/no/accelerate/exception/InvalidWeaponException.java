package no.accelerate.exception;

public class InvalidWeaponException extends Throwable {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
