package no.accelerate;

import no.accelerate.characters.Warrior;
import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;


public class Main {
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {
        // Creating character, weapons and armor
        Warrior warrior = new Warrior("Warru");
        Weapon weapon = new Weapon("Super Rare Axe", 10,5, WeaponType.AXE, Slot.ARM);
        Armor armor = new Armor("Common Head Mail", 1, ArmorType.MAIL, Slot.HEAD);
        Armor armorBody = new Armor("Common Body Mail", 1, ArmorType.MAIL, Slot.BODY);
        Armor armorLeg = new Armor("Common Leg Mail", 1, ArmorType.MAIL, Slot.LEG);
        // Leveling up to 5
        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();
        // Equipping all the items
        warrior.equipWeapon(weapon);
        warrior.equipArmor(armor);
        warrior.equipArmor(armorBody);
        warrior.equipArmor(armorLeg);
        // Calculating total DPS
        warrior.dpsWithItems(weapon, armor);
        // Printing out statistics using StringBuilder
        StringBuilder example = new StringBuilder();
        example.append("Name = " + warrior.getName() + "\n");
        example.append("Level = " + warrior.getLevel() + "\n");
        example.append("Strength = " + warrior.getBaseAttributes().getStrength() + "\n");
        example.append("Dexterity = " + warrior.getBaseAttributes().getDexterity() + "\n");
        example.append("Intelligence = " + warrior.getBaseAttributes().getIntelligence() + "\n");
        example.append("Total DPS = " + warrior.getCharacterDps().getTotalDps() + "\n");
        example.toString();
        System.out.println(example);

    }
}
