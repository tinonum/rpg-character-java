package no.accelerate.characters;

public class CharacterDps {
    private double totalDps;

    public CharacterDps(double totalDps) {
        this.totalDps = totalDps;
    }

    /* Generated methods */

    // Getters and setters
    public double getTotalDps() {
        return totalDps;
    }

    public void setTotalDps(double totalDps) {
        this.totalDps = totalDps;
    }
}
