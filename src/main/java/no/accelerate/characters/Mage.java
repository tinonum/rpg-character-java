package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import java.text.DecimalFormat;

import static no.accelerate.item.ArmorType.*;
import static no.accelerate.item.WeaponType.*;

public class Mage extends PlayableCharacter{
    public Mage(String name) {
        super("Maagi", 1, 1, 1, 8,1);
    }

    // Level up and its attribute enhancements
    @Override
    public void levelUp() {
        super.levelUp();
        super.baseAttributes.setStrength(baseAttributes.getStrength() + 1);
        super.baseAttributes.setDexterity(baseAttributes.getDexterity() + 1);
        super.baseAttributes.setIntelligence(baseAttributes.getIntelligence() + 5);
    }

    // Determine Mage DPS without items
    @Override
    public void dpsWithoutItems() {
        double doubleMainAttribute = (baseAttributes.getIntelligence() / 100.0);
        super.characterDps.setTotalDps(characterDps.getTotalDps() + doubleMainAttribute);
    }

    // Determine Mage DPS with items, and checking that no invalid weapons are being equipped
    @Override
    public void dpsWithItems(Weapon weapon, Armor armor) throws InvalidWeaponException, InvalidArmorException {
        if (weapon.getWeaponType() != STAFF && weapon.getWeaponType() != WAND) {
            throw new InvalidWeaponException("Mage cannot use that weapon or cannot equip it in this slot!");
        }
        if (weapon.getWeaponType() == WAND && getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Character level too low!");
        }
        if (weapon.getSlot() != Slot.ARM) {
            throw new InvalidWeaponException("Cannot equip weapon in this slot!");
        }
        if (armor.getArmorType() != CLOTH) {
            throw new InvalidArmorException("Mage cannot use that armor!");
        }
        if (armor.getArmorType() == CLOTH && getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Character level too low!");
        }
        double weaponDps = ((weapon.getDamage() * 1.1) * ((characterDps.getTotalDps()) + (baseAttributes.getIntelligence()) / 100.0));
        // Using DecimalFormat to round dps to two decimals
        DecimalFormat twoDecimals = new DecimalFormat("##.##");
        double roundedDps = Double.parseDouble(twoDecimals.format(weaponDps));
        super.characterDps.setTotalDps(roundedDps);
    }

    // Equipping weapon, throwing exception if weapon cannot be used
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getWeaponType() != STAFF && weapon.getWeaponType() != WAND) {
            throw new InvalidWeaponException("Mage cannot use that weapon!");
        }
        if (weapon.getWeaponType() == WAND && getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Character level too low!");
        }
        if (weapon.getSlot() != Slot.ARM) {
            throw new InvalidWeaponException("Cannot equip weapon in this slot!");
        }
        super.equipWeapon(weapon);
    }

    // Equipping armor, throwing exception if armor cannot be used
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType() != CLOTH) {
            throw new InvalidArmorException("Mage cannot use that armor!");
        }
        if (armor.getArmorType() == CLOTH && getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Character level too low!");
        }
        if (armor.getSlot() == Slot.ARM) {
            throw new InvalidArmorException("Cannot equip Armor in this slot!");
        }
        super.equipArmor(armor);
    }
}
