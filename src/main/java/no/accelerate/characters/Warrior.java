package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;
import java.text.DecimalFormat;

import static no.accelerate.item.ArmorType.*;
import static no.accelerate.item.WeaponType.*;

public class Warrior extends PlayableCharacter{
    public Warrior(String name) {
        super("Warru", 1,5,2,1,1);
    }

    // Leveling up and enhancing attributes
    @Override
    public void levelUp() {
        super.levelUp();
        super.baseAttributes.setStrength(baseAttributes.getStrength() + 3);
        super.baseAttributes.setDexterity(baseAttributes.getDexterity() + 2);
        super.baseAttributes.setIntelligence(baseAttributes.getIntelligence() + 1);
    }

    // Determine Warrior DPS without items
    @Override
    public void dpsWithoutItems() {
        double doubleMainAttribute = (baseAttributes.getStrength() / 100.0);
        super.characterDps.setTotalDps(characterDps.getTotalDps() + doubleMainAttribute);
    }

    // Determine Warrior DPS with items, and checking that no invalid weapons are being equipped
    @Override
    public void dpsWithItems(Weapon weapon, Armor armor) throws InvalidWeaponException, InvalidArmorException {
        if (weapon.getWeaponType() != AXE && weapon.getWeaponType() != HAMMER && weapon.getWeaponType() != SWORD) {
            throw new InvalidWeaponException("Warrior cannot use that weapon!");
        }
        if (weapon.getWeaponType() == AXE && getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Character level too low!");
        }
        if (weapon.getSlot() != Slot.ARM) {
            throw new InvalidWeaponException("Cannot equip weapon in this slot!");
        }
        if (armor.getArmorType() != MAIL && armor.getArmorType() != PLATE) {
            throw new InvalidArmorException("Warrior cannot use that armor!");
        }
        if (armor.getArmorType() == PLATE && getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Character level too low!");
        }
        if (armor.getSlot() == Slot.ARM) {
            throw new InvalidArmorException("Cannot equip Armor in this slot!");
        }
        double weaponDps = ((weapon.getDamage() * 1.1) * ((characterDps.getTotalDps()) + (baseAttributes.getStrength()) / 100.0));
        // Using DecimalFormat to round dps to two decimals
        DecimalFormat twoDecimals = new DecimalFormat("##.##");
        double roundedDps = Double.parseDouble(twoDecimals.format(weaponDps));
        super.characterDps.setTotalDps(roundedDps);
    }

    // Equipping weapon, throwing exception if item cannot be used
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getWeaponType() != AXE && weapon.getWeaponType() != HAMMER && weapon.getWeaponType() != SWORD) {
            throw new InvalidWeaponException("Warrior cannot use that weapon!");
        }
        if (weapon.getWeaponType() == AXE && getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Character level too low!");
        }
        if (weapon.getSlot() != Slot.ARM) {
            throw new InvalidWeaponException("Cannot equip weapon in this slot!");
        }
        super.equipWeapon(weapon);
    }

    // Equipping armor, throwing exception if item cannot be used
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType() != MAIL && armor.getArmorType() != PLATE) {
            throw new InvalidArmorException("Warrior cannot use that armor!");
        }
        if (armor.getArmorType() == PLATE && getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Character level too low!");
        }
        if (armor.getSlot() == Slot.ARM) {
            throw new InvalidArmorException("Cannot equip Armor in this slot!");
        }
        super.equipArmor(armor);
    }
}
