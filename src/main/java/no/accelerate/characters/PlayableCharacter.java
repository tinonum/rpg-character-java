package no.accelerate.characters;

import no.accelerate.exception.InvalidArmorException;
import no.accelerate.exception.InvalidWeaponException;
import no.accelerate.item.*;

import java.util.HashMap;

public abstract class PlayableCharacter {
    private String name;
    private int level;
    protected PrimaryAttributes baseAttributes;
    protected CharacterDps characterDps;


    // Constructor
    public PlayableCharacter(String name, int level, double strength, double dexterity, double intelligence, double baseDps) {
        this.name = name;
        this.level = level;
        this.baseAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
        this.characterDps = new CharacterDps(baseDps);
    }

    // Level up method
    public void levelUp() {
        level += 1;
    }

    // Determine DPS without items
    public void dpsWithoutItems() {
        characterDps = getCharacterDps();
    }

    // Determine DPS with items
    public void dpsWithItems (Weapon weapon, Armor armor) throws InvalidWeaponException, InvalidArmorException {
        characterDps = getCharacterDps();
    }

    // HashMap to store equipment/items
    HashMap<Slot, Item> equipmentMap = new HashMap<>();

    // Equipping weapons or throwing Exception if the item is wrong. Also storing them in HashMap.
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        equipmentMap.put(Slot.ARM, weapon);
    }

    // Equipping armor or throwing Exception if the item is wrong. Also storing them in HashMap.
    public void equipArmor(Armor armor) throws InvalidArmorException {
        baseAttributes.setStrength(baseAttributes.getStrength() + armor.getExtraHealth());
        equipmentMap.put(Slot.HEAD, armor);
    }

    /* Generated methods */

    // toString() to pass to main to be printed
    @Override
    public String toString() {
        return "PlayableCharacter{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", baseAttributes=" + baseAttributes +
                ", characterDps=" + characterDps +
                ", equipmentMap=" + equipmentMap +
                '}';
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    public void setBaseAttributes(PrimaryAttributes baseAttributes) {
        this.baseAttributes = baseAttributes;
    }

    public CharacterDps getCharacterDps() {
        return characterDps;
    }

    public void setCharacterDps(CharacterDps characterDps) {
        this.characterDps = characterDps;
    }

    public HashMap<Slot, Item> getEquipmentMap() {
        return equipmentMap;
    }

    public void setEquipmentMap(HashMap<Slot, Item> equipmentMap) {
        this.equipmentMap = equipmentMap;
    }
}
